package model;

import java.util.List;

public class AdvancedPerson extends Person {

    private boolean hasMagic;
    private List<String> skills;

    public AdvancedPerson  (String name, int age, boolean hasMagic, List<String> skills) {
        super(name, age);
        this.hasMagic = hasMagic;
        this.skills = skills;
    }

    @Override
    protected String niceString() {
        StringBuilder sb = new StringBuilder();
        if (hasMagic) {
            sb.append(", with magical abilities: \n\t");
            for (String skill : skills) {
                sb.append(skill + ", ");
            }
        } else {
            sb.append(", without magical abilities");
        }
        return sb.toString();
    }
}
