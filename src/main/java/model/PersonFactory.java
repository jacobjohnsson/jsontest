package model;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.List;
import java.util.LinkedList;

import utility.FileHandling;

public interface PersonFactory {
    
    public default List<Person> fromFile (String filePath) {
        String fileAsString = FileHandling.fileToString(filePath);
        JSONObject JSONpersons = new JSONObject(fileAsString);

        return fromJSON(JSONpersons);
    }

    public default List<Person> fromJSON (JSONObject object) {
        JSONArray array = object.getJSONArray("persons");
        List<Person> persons = new LinkedList<Person>();
        for (Object o : array) {
            JSONObject p = (JSONObject) o;
            persons.add(createPerson(p));
        }

        return persons;
    }

    public Person createPerson (JSONObject p);
}
