package model;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.List;
import java.util.LinkedList;

public class AdvancedPersonFactory implements PersonFactory {

    @Override
    public Person createPerson (JSONObject p) {
        String name = p.getString("name");
        int age = p.getInt("age");
        boolean hasMagic = p.getBoolean("magic");

        List<String> skills = new LinkedList<String>();
        JSONArray JSONSkills = p.getJSONArray("skills");
        for (Object o : JSONSkills) {
            String skill = (String) o;
            skills.add(skill);
        }

        return new AdvancedPerson(name, age, hasMagic, skills);
    }
}
