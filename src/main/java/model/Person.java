package model;

public abstract class Person {
    protected String name;
    protected int age;

    public Person  (String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public final String toString() {
        return name + ", " + age + niceString() + ".";
    }

    protected String niceString() {
        return "";
    }
}
