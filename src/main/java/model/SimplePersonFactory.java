package model;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.List;
import java.util.LinkedList;

public class SimplePersonFactory implements PersonFactory {

    @Override
    public Person createPerson (JSONObject p) {
        String name = p.getString("name");
        int age = p.getInt("age");
        
        return new SimplePerson(name, age);
    }
}
