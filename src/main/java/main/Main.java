package main;

import java.io.IOException;
import java.util.List;

import config.Configuration;
import config.CustomConfiguration;
import utility.FileHandling;
import model.Person;
import model.PersonFactory;

import org.json.JSONObject;

public class Main {
    private PersonFactory pf;
    private String path;

    public Main  () {
        Configuration config = new CustomConfiguration();
        pf = config.factory();
        path = config.pathToPersons();
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.printPersons();
    }

    private void printPersons() {
        List<Person> persons = pf.fromFile(path);
        for (Person p : persons) {
            System.out.println(p);
        }
    }
}
