package config;

import model.PersonFactory;
import model.AdvancedPersonFactory;
import model.SimplePersonFactory;
import utility.FileHandling;

import org.json.JSONObject;

public class CustomConfiguration implements Configuration {

    protected String path;
    protected PersonFactory factory;

    public CustomConfiguration  () {
        readConfig();
    }

    private void readConfig() {
        String configPath = "src/main/resources/config.json";
        String configFileAsString = FileHandling.fileToString(configPath);
        JSONObject configFile = new JSONObject(configFileAsString);
        String type = configFile.getString("type").toLowerCase();

        switch (type) {
            case "advanced":
                factory = new AdvancedPersonFactory();
                path = "src/main/resources/AdvancedPersons.json";
                break;
            default :
                factory = new SimplePersonFactory();
                path = "src/main/resources/SimplePersons.json";
                break;
        }
    }

    public PersonFactory factory() {
        return factory;
    }

    public String pathToPersons() {
        return path;
    }
}
