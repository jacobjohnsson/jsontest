package config;

import model.PersonFactory;
import model.AdvancedPersonFactory;
import model.SimplePersonFactory;
import utility.FileHandling;

import org.json.JSONObject;

public class DefaultConfiguration implements Configuration {

    public PersonFactory factory() {
        return new SimplePersonFactory();
    }

    public String pathToPersons() {
        return "src/main/resources/SimplePersons.json";
    }
}
