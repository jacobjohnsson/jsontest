package config;

import model.PersonFactory;
import model.AdvancedPersonFactory;
import model.SimplePersonFactory;
import utility.FileHandling;

import org.json.JSONObject;

public interface Configuration {

    public PersonFactory factory();

    public String pathToPersons();
}
