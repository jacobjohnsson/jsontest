package utility;

import java.io.File;
import java.util.Scanner;

public class FileHandling {
    public static String fileToString (String path) {
        File file = new File(path);
        StringBuilder contents = new StringBuilder();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                contents.append(scanner.nextLine() + System.lineSeparator());
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return contents.toString();
    }

}
