Jacob Johnsson

## Att använda JSON för konfigurering

Det är möjligt att använda JSON för konfigureringsfiler (config), även om det inte är rekommenderat. Det största argumentet jag hittat emot JSON för configfiler är att det inte stödjer kommentarer. Fördelen med JSON är att det verkar vara branschens standard för kommunikation mellan klient och server i "lätta" program. Om vi blir bekanta med det redan nu kanske det kan visa sig underlätta implementationen av Client-Server. Alternativet är nog att implementera allt detta själva.

### Vad är JSON?

JSON är ett sätt att modellera och spara objekt i textdokument. Ex:


```json
{
  "name": "goblin grunt",
  "minHealth": 20,
  "maxHealth": 30,
  "resists": ["cold", "poison"],
  "weaknesses": ["fire", "light"]
}
```
Kanske lite krystat exempel. Tanken är sedan att programmet kan "slå upp" information i filen under körtid. I vårt fall blir det kanske något i stil med

```json
{
    "raceType": "Maraton",
    "namnFil": "NamnFil.txt",
    "startFil": "StartFil.txt",
    "maxVarv": 3,
    "minTid": 15,
    "ärMassStart": true,
    "harKlasser": false,
    "klasser": ["Junior", "Senior"]
}
```

Detta går att snygga till genom att följa Prototype pattern: http://www.gameprogrammingpatterns.com/prototype.html (godbitarna kommer längst ner).

### Hur funkar det?

Det finns [ett projekt](https://gitlab.com/jacobjohnsson/jsontest), ett litet exempel, på hur .json kan användas för att konfigurera hur ett program ska bete sig. Idén är att låta en separat klass läsa configfilen och returnera objekt av korrekt typ som matchar behoven som ställs av configfilen. Klassen som huvudprogrammet tar hjälp av i exempelprojektet av heter CustomConfiguration. Det API som använts är samma som kommer med Android SDK, **org.json**, det verkar fungera bra för våra ändamål.

Det viktigaste att förstå är att vid körtid är json-objekten av typen JSONObject som tar hela filen som en sträng som argument till konstruktorn. På JSONObject kan man sedan göra anrop:
```java
JSONObject configFile = new JSONObject(configFileAsString);
String type = configFile.getString("raceType").toLowerCase();

switch (type) {
    case "maraton":
        // instansiera ett maratonlopp.
        break;
    case "varv":
        // instansiera ett varvlopp.
        break;
    case "etapp":
        // instansiera ett etapplopp.
        break;
}
```
[org.json dokumenation](https://stleary.github.io/JSON-java/)

Dokumentationen kan vara dryg att läsa om man vill få en överblick. För att sammanfatta den kan man se alla JSON-saker som antingen är JSONObject (ett enda objekt) eller JSONArray (se "klasser" i exemplet ovan). För att sedan extrahera informationen ur ett JSONObject kan man använda anrop som .getString() (se ovan), .getInt(), .getBoolean() etc. JSONArrays översätts lämpligtvis element för element till en lista.
Ex:
```java
List<String> skills = new LinkedList<String>();
JSONArray JSONSkills = p.getJSONArray("skills");
for (Object o : JSONSkills) {
    String skill = (String) o;
    skills.add(skill);
}
```


Känner man att man inte ser hur detta kan hänga ihop med hur ett program konfigureras så kolla igenom projektet, jag känner att jag fick till en ganska robust design.

### Överkurs
Det går också att nästla JSONObject. Jag tror inte att detta kommer bli relevant i vårt projekt, men skulle kunna användas för att ha en fil med fördefinierade typer av lopp. Den filen skulle kunna innehålla mallar för olika lopp, som sedan refereras till genom config-filen. Filerna skulle alltså kunna se ut enl:

config-fil:
```json
{
    "raceType": "StandardVarv",
    "namnFil": "NamnFil.txt",
    "startFil": "StartFil.txt",
}
```

standardlopp-fil:
```json
{   
    "StandardVarv": {
        "maxVarv": 3,
        "minTid": 15,
        "ärMassStart": false,
        "harKlasser": false
    },

    "StandardMaraton": {
        "ärMassStart": false,
        "harKlasser": false
    }
}
```

Tror att detta borde gå att implementera med något liknande prototype-pattern blandat med Factory Pattern, men fuck it, mina 4h är över ;).
